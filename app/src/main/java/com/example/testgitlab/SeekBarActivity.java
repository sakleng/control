package com.example.testgitlab;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.ToggleButton;

public class SeekBarActivity extends AppCompatActivity {

    @BindView(R.id.seekBar)
    SeekBar seekBar;
    @BindView(R.id.tgBtn)
    ToggleButton tgBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seek_bar);

        ButterKnife.bind(this);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.e("@@@@@",String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.e("@@@@@","Accessing StartTracking!");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.e("@@@@@","Accessing StopTracking!");
            }
        });

        tgBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    Toast.makeText(SeekBarActivity.this,"Turn ON",Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(SeekBarActivity.this,"Turn OFF", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
