package com.example.testgitlab;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;

public class ManuActivity extends AppCompatActivity {

    @BindView(R.id.ctMenu)
    Button button;
    @BindView(R.id.ctMenu2)
    Button btnSubj;
    @BindView(R.id.ctMenu3)
    Button btnFruit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manu);

        ButterKnife.bind(this);

        registerForContextMenu(button);
        registerForContextMenu(btnSubj);

        btnFruit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(ManuActivity.this ,v);
                popupMenu.inflate(R.menu.pop_up_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.smBanana:
                                Toast.makeText(ManuActivity.this,"You Selected Banana",Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.smApple:
                                Toast.makeText(ManuActivity.this,"You Selected Apple",Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.smOrange:
                                Toast.makeText(ManuActivity.this,"You Selected Orange",Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.smMango:
                                Toast.makeText(ManuActivity.this,"You Selected Mango",Toast.LENGTH_SHORT).show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu menu) {
                        Toast.makeText(ManuActivity.this,"Pick Something, You piece of shit!!!",Toast.LENGTH_SHORT).show();
                    }
                });
                popupMenu.show();

            }
        });

        btnFruit.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(ManuActivity.this,"You Press me so long dude !",Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_manu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.insert :
                Toast.makeText(this,"Insert Data",Toast.LENGTH_SHORT).show();
                break;
            case R.id.delete :
                Toast.makeText(this,"Delete Data",Toast.LENGTH_SHORT).show();
                break;
            case R.id.share :
                Toast.makeText(this,"Share Data",Toast.LENGTH_SHORT).show();
                break;
            case R.id.upload :
                Toast.makeText(this,"Upload Data",Toast.LENGTH_SHORT).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        switch (v.getId()){
            case R.id.ctMenu:
                getMenuInflater().inflate(R.menu.context_menu,menu);
                menu.setHeaderTitle("Choose Optional");
                break;
            case R.id.ctMenu2:
                getMenuInflater().inflate(R.menu.subject_menu,menu);
                menu.setHeaderTitle("Choose Subject");
                break;
            case R.id.ctMenu3:
                getMenuInflater().inflate(R.menu.pop_up_menu,menu);
                menu.setHeaderTitle("Choose Fruit");
                break;
        }

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.opt1:
                Toast.makeText(this,"You Selected Option 1",Toast.LENGTH_SHORT).show();
                break;
            case R.id.opt2:
                Toast.makeText(this,"You Selected Option 2",Toast.LENGTH_SHORT).show();
                break;
            case R.id.opt3:
                Toast.makeText(this,"You Selected Option 3",Toast.LENGTH_SHORT).show();
                break;
            case R.id.opt4:
                Toast.makeText(this,"You Selected Option 4",Toast.LENGTH_SHORT).show();
                break;
            case  R.id.smJava:
                Toast.makeText(this,"You Selected Java",Toast.LENGTH_SHORT).show();
                break;
            case R.id.smAndroid:
                Toast.makeText(this,"You Selected Android",Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onContextItemSelected(item);
    }
}
