package com.example.testgitlab;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class AnimationActivity extends AppCompatActivity {

    @BindView(R.id.imgFB)
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);

        ButterKnife.bind(this);

        Animation animation = AnimationUtils.loadAnimation(this,R.anim.translate);
        imageView.setAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Log.e("@@@@@@",">>>>> Start");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.e("@@@@@@",">>>>> End");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.e("@@@@@@",">>>>> Repeat");
            }
        });

    }
}
