package com.example.testgitlab;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.spinner)
    Spinner spSubject;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        final List<Subject> listSubj = new ArrayList<>();
        listSubj.add(new Subject(1,"Java"));
        listSubj.add(new Subject(2,"Android"));
        listSubj.add(new Subject(3,"React Native"));
        listSubj.add(new Subject(4,"Web"));
        listSubj.add(new Subject(5,"Security Hacking"));
        listSubj.add(new Subject("-- SELECT SUBJECT --"));

        ArrayAdapter<Subject> adapter = new ArrayAdapter<Subject>(this,android.R.layout.simple_list_item_1,listSubj){
            @Override
            public int getCount() {
                int count = super.getCount();
                return (count  > 0) ? count - 1 : count;
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSubject.setAdapter(adapter);
        spSubject.setSelection(adapter.getCount());

        spSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Subject sub = (Subject) parent.getSelectedItem();
                Toast.makeText(MainActivity.this,sub.getId()+"",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }




}
