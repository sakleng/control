package com.example.testgitlab;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ViewScreen extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.txtResult)
    TextView txtResult ;
    @BindView(R.id.btnFB)
    Button btnFB;
    @BindView(R.id.btnBS)
    Button btnBS;
    @BindView(R.id.btnOther)
    Button btnOther;
    @BindView(R.id.rdGroup)
    RadioGroup rdGroup;
    @BindView(R.id.cbAndroid)
    CheckBox cbAndroid;
    @BindView(R.id.cbJava)
    CheckBox cbJava;
    @BindView(R.id.cbHtml)
    CheckBox cbHtml;

    List<String> sub;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_screen);

        ButterKnife.bind(this);
        sub = new ArrayList<>();

//        rdGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                String gender = "";
//                if (checkedId == R.id.rdMale) gender = "Male";
//                else gender = "Female";
//                txtResult.setText(gender);
//            }
//        });

        cbAndroid.setOnCheckedChangeListener(this);
        cbJava.setOnCheckedChangeListener(this);
        cbHtml.setOnCheckedChangeListener(this);
    }

//    public void btnOther(View view) {
//        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
//        try {
//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.ss.android.ugc.trill")));
//        } catch (android.content.ActivityNotFoundException anfe) {
//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "sg.bigo.live.lite")));
//        }
//    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        String subj = buttonView.getText().toString();
        if (isChecked)
            sub.add(subj);
        else
            sub.remove(subj);

        for (String items : sub){
            Log.e("@@@@@@@",items);
        }
    }
}
